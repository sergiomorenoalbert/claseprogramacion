package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Controller.PrincipalController;

public class Principal {

	public JFrame frame;


	public Principal() {
		initialize();
	}

	private void initialize() {
		PrincipalController pc = new PrincipalController(); 
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		
		JLabel lblActividadDePrueba = new JLabel("ACTIVIDAD DE PRUEBA");
		lblActividadDePrueba.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblActividadDePrueba.setAlignmentX(Component.CENTER_ALIGNMENT);
		frame.getContentPane().add(lblActividadDePrueba);
		
		Component verticalGlue = Box.createVerticalGlue();
		frame.getContentPane().add(verticalGlue);
		
		JButton btnNewButton = new JButton("IR A EXPORTADOR");
		
		//Método que comprueba si el usuario intenta ir a otro lado
		pc.goToExportador(btnNewButton);
		
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		frame.getContentPane().add(btnNewButton);
	}

}
