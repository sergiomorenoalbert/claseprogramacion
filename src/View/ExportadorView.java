package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import Controller.ExportadorController;
import Controller.ConsultasController;
import javax.swing.JList;

public class ExportadorView {

	public JFrame frmExportadorDePdf;

	/**
	 * Create the application.
	 */
	public ExportadorView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		ExportadorController ec = new ExportadorController();
		ConsultasController cc = new ConsultasController();

		frmExportadorDePdf = new JFrame();
		frmExportadorDePdf.setTitle("EXPORTADOR DE PDF");
		frmExportadorDePdf.setResizable(false);
		frmExportadorDePdf.setBounds(100, 100, 450, 300);
		frmExportadorDePdf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmExportadorDePdf.getContentPane().setLayout(null);
		
		JButton btnExportarAPdf = new JButton("EXPORTAR A PDF");
		btnExportarAPdf.setBounds(142, 218, 155, 43);
		frmExportadorDePdf.getContentPane().add(btnExportarAPdf);
		DefaultListModel model = new DefaultListModel();
		HashMap<String, String> resultado = cc.listarCursos();
		for(Iterator iter = resultado.keySet().iterator(); iter.hasNext();){
			model.addElement(resultado.get(iter.next()));
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(80, 6, 300, 200);
		frmExportadorDePdf.getContentPane().add(scrollPane);
		JList list = new JList(model);
		scrollPane.setViewportView(list);
		
		ec.getTextArea(btnExportarAPdf, model);
		/*ESTO ESTA REALMENTE EN EL CONTROLADOR
		ec.imprimeHolas(btnExportarAPdf);*/
	}
}
