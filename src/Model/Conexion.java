package Model;
import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

	private Connection connexio;
	
	public Conexion() {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		}catch(Exception e){
			System.err.println("Error registrant el Driver mysql");
			System.err.println(e);
		}
		try{
			String cadenaDeConnexio = "jdbc:mysql://localhost/educa?user=root&password=";
			connexio= DriverManager.getConnection(cadenaDeConnexio);
		}catch(Exception e){
			System.err.println("Error connectant a mysql/educa amb user=root");
		}
	
	}

	public Connection getConnexio() {
		return connexio;
	}
	public void setConnexio(Connection connexio) {
		this.connexio = connexio;
	}
	
	
}
