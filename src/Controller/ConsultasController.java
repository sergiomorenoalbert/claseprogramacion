package Controller;

import java.awt.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import Model.Conexion;

public class ConsultasController {

	public ConsultasController() {
		// TODO Auto-generated constructor stub
	}

	public HashMap<String, String> listarCursos(){
		HashMap<String, String> resultado = new HashMap<String,String>();
		ResultSet resultatConsulta = null;
		Conexion cn = new Conexion();
		try {
			Statement stmt=cn.getConnexio().createStatement();		
			resultatConsulta = stmt.executeQuery("SELECT Cno,Cnombre FROM CURSO");
			while(resultatConsulta.next()){
				String Cno = resultatConsulta.getString("Cno");
				String Cnombre = resultatConsulta.getString("Cnombre");
				resultado.put(Cno,Cnombre);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultado;
	}
}
