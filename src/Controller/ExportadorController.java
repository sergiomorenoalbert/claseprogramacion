package Controller;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class ExportadorController {
	private String content;
	public ExportadorController() {
		this.content = "";
	}
	
	public void getTextArea(JButton btnExportarAPdf,DefaultListModel textArea){
		btnExportarAPdf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportarPDF(textArea.toString());
			}
		});
	}
	
	/*public void imprimeHolas(JButton x){
		x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("HOLAS");
			}
		});
	}*/
	
	
	public void exportarPDF(String texto){
		this.content = texto;
		String fichero = "exportacion1.txt";
		File x = new File(fichero);
		if(!x.exists()){
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));
				bw.write(this.content);
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			System.out.println("Exportaciaoooo erronea porque ya tienes esa fichero");
		}
		
	}
	

}
