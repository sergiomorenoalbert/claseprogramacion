package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import View.ExportadorView;

public class PrincipalController {

	public PrincipalController() {
		
	}
	/*
	 * Método to guapo que dirige a la vista Exportador
	 */
	public static void goToExportador(JButton btnNewButton){
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExportadorView window = new ExportadorView();
				window.frmExportadorDePdf.setVisible(true);
			}
		});
	}

}
